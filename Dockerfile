ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_opt_bcftools:1.10.2 as opt_bcftools

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_tools:19.04 AS build

RUN mkdir -p /tmp/scratch/build && \
    cd /tmp/scratch && wget -t 1 http://localhost:8000/snpEff_latest_core.zip || \
                       wget -t 3 https://osdn.net/frs/g_redir.php?m=kent&f=snpeff%2FsnpEff_latest_core.zip && \
    cd /tmp/scratch && unzip snpEff_latest_core.zip && mv snpEff /opt/ && \
    cd / && rm -rf /tmp/scratch

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_opt_openjdk:8u

COPY --from=opt_bcftools /opt /opt
COPY --from=build /opt /opt

RUN mkdir -p /opt/bin/ && echo "#!/bin/bash" > /opt/bin/module && chmod a+x /opt/bin/module

RUN mkdir -p /opt/bin/ && \
    echo -e '#!/bin/bash\njava -Xmx8G -jar /opt/snpEff/snpEff.jar $@' > /opt/bin/snpEff && chmod +x /opt/bin/snpEff
